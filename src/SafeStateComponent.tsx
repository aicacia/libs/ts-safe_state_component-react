import { PureComponent } from "react";

export abstract class SafeStateComponent<P, S> extends PureComponent<P, S> {
  private _isMounted: boolean;

  constructor(props: P) {
    super(props);

    this._isMounted = false;
  }
  safeForceUpdate(callBack?: () => void) {
    if (this._isMounted) {
      this.forceUpdate(callBack);
    }
  }
  safeSetState<K extends keyof S>(
    state:
      | ((prevState: Readonly<S>, props: Readonly<P>) => Pick<S, K> | S | null)
      | (Pick<S, K> | S | null),
    callback?: () => void
  ) {
    if (this._isMounted) {
      this.setState(state, callback);
    }
  }
  componentIsMounted() {
    return this._isMounted;
  }
  componentDidMount() {
    this._isMounted = true;
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
}
