import * as Enzyme from "enzyme";
import * as EnzymeAdapter from "enzyme-adapter-react-16";
// @ts-ignore
import { JSDOM } from "jsdom";
import * as React from "react";
import * as tape from "tape";
import { SafeStateComponent } from ".";

const dom = new JSDOM("<!doctype html><html><body></body></html>");

(global as any).document = dom.window.document;
(global as any).window = dom.window;

Enzyme.configure({ adapter: new EnzymeAdapter() });

interface ITestProps {}
interface ITestState {
  current: string;
}

class Test extends SafeStateComponent<ITestProps, ITestState> {
  state: ITestState = {
    current: "",
  };

  render() {
    return this.state.current;
  }
}

const run = (fn: () => void, ms = 0) =>
  new Promise((resolve) => {
    setTimeout(() => {
      fn();
      resolve();
    }, ms);
  });

tape("SafeStateComponent", async (assert: tape.Test) => {
  const wrapper = Enzyme.mount(<Test />),
    component = wrapper.instance() as Test;

  assert.equal(wrapper.getDOMNode().textContent, "");

  component.safeSetState({ current: "safe-set" });
  assert.equal(wrapper.getDOMNode().textContent, "safe-set");

  const result = run(() => {
    component.safeSetState({ current: "never-set" });
  });

  wrapper.unmount();
  await result;

  assert.end();
});
